package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadController {
    private static final Logger log = LoggerFactory.getLogger(UploadController.class);

    @GetMapping("/upload-form")
    public String uploadForm() {
        return "uploadForm";
    }

    @PostMapping("/upload")
    @ResponseBody
    public String handleFileUpload(@RequestParam("file") MultipartFile file) {

        log.info("File {} successfully uploaded", file.getOriginalFilename());
        return file.getOriginalFilename();
    }

}
